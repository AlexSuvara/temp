<?php

declare(strict_types=1);

namespace App\Entity\Booking;

use App\Antity\Acquiring\Test\UpcAcquiring;
use App\Entity\CurrencyRatio;
use App\Entity\Passenger\Passenger;
use App\Entity\Payment\Order\Passenger\Payment;
use App\Entity\Port;
use App\Entity\Agent\Agent;
use App\Entity\Tariff\Passenger\Tariff;
use App\Entity\User\User;
use App\Entity\Vehicle\Personal;
use App\Entity\Voyage\Voyage;
use App\Entity\Tariff\Vehicle\Personal\UseCase;
use App\Entity\Settings;
use App\Entity\Voyage\VoyagesPortsPivot;
use App\ReadModel\Booking\PassengerBookingFetcher;
use Carbon\Carbon;

class PassengerBooking extends Booking implements BookingInterface
{
    public const INTERNAL_PURPOSES_BOOKING_CONFIRMATION_EMAILS = 'develop-test@ukr.net,rma@ukrferry.com';

    public function passenger()
    {
        return $this->hasMany(Passenger::class, 'booking_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function upcAcquiring()
    {
        return $this->hasMany(UpcAcquiring::class, 'booking_id');
    }

    public function boarding()
    {
        return $this->belongsTo(Port::class, 'boarding_port');
    }

    public function disembarking()
    {
        return $this->belongsTo(Port::class, 'disembarking_port');
    }

    public function voyage()
    {
        return $this->belongsTo(Voyage::class, 'voyage_id')->with('ship');
    }

    public function vehicle()
    {
        return $this->hasMany(Personal::class);
    }

    public static function getByOrder(string $orderBatch)
    {
        $booking = self::where('order_batch', $orderBatch)->first();

        return $booking;
    }

    public static function isStatusAsOccupiedPlaces($status) {
        return in_array($status, [self::ACTIVE_STATUS]);
    }

    public static function existsWithCabin(int $cabin, int $voyage)
    {
        return self::where('voyage_id', $voyage)
            ->where('type', Booking::PASSENGER_BOOKING_TYPE)
            ->where('status', Booking::ACTIVE_STATUS)
            ->whereExists(function ($q) use ($cabin) {
                $q->select(\DB::raw(1))
                    ->from('passengers')
                    ->whereRaw('bookings.id = passengers.booking_id')
                    ->where('passengers.cabin_id', $cabin);
            })
            ->get()
            ->isNotEmpty();
    }

    private static function generateNumber()
    {
        $tryNumber = \Str::random(6);
        if (self::where('number', $tryNumber)->first()) {
            return self::generateNumber();
        }
        return $tryNumber;
    }

    public static function createByOrder(array $order)
    {
        return self::add([
            'status' => self::UNPAID_STATUS,
            'type' => self::PASSENGER_BOOKING_TYPE,
            'number' => self::generateNumber(),
            'user_id' => \Auth::user()->id,
            'order_batch' => $order['batch'],
            'voyage_id' => $order['voyage']['id'],
            'boarding_port' => $order['boarding_id'],
            'disembarking_port' => $order['disembarking_id'],
        ]);
    }

    public static function add($data)
    {
        $booking = self::create([
            'status' => $data['status'],
            'type' => $data['type'],
            'number' => $data['number'],
            'user_id' => $data['user_id'],
            'order_batch' => $data['order_batch'],
            'voyage_id' => $data['voyage_id'],
            'boarding_port' => $data['boarding_port'],
            'disembarking_port' => $data['disembarking_port'],
        ]);

        $booking->save();

        return $booking;
    }

    public function bindPersonalVehicle(array $data)
    {
        $driverPassenger = Passenger::where('id', $data['driver'])
            ->where('booking_id', $this->id)->first();

        if ($driverPassenger->hasPersonalVehicleOnVoyage($this->voyage_id)) {
            throw new \DomainException('This passenger already has personal vehicle.');
        }

        $command = new UseCase\Calculate\Command(
            [
                'voyage' => $this->voyage_id,
                'ship_id' => $this->voyage->ship->id,
                'driver_booking_number' => $data['booking_number'],
                'vehicle_type' => $data['vehicle_type'],
                'length' => $data['length'],
                'weight' => $data['weight'],
                'passenger_id' => $data['driver'],
                'currency' => $data['currency']
            ]
        );

        $handler = new UseCase\Calculate\Handler();
        $tariff = $handler->handle($command);

        $personalVehicle = [
            'sales_method' => empty($data['sales_method']) ? 'online' : $data['sales_method'],
            'booking_id' => $this->id,
            'vehicle_id' => $data['vehicle_type'],
            'length' => $data['length'],
            'weight' => $data['weight'],
            'vehicle_make' => $data['vehicle_make'],
            'vehicle_model' => $data['vehicle_model'],
            'vin' => $data['vin'],
            'registration_number' => $data['reg_number'],
            'driver' => $data['driver'],
            'tariff' => $tariff['amount'],
            'currency' => $data['currency'],
            'base_currency' => Tariff::BASE_CURRENCY,
            'ratio_to_base_currency' => CurrencyRatio::getCurrencyRatio($data['currency'], Tariff::BASE_CURRENCY),
            'discounts' => $tariff['discountAmount'],
            'base_tariff_id' => $tariff['base_tariff_id']
        ];
        $personalVehicle = Personal::create($personalVehicle);
        $personalVehicle->save();
    }

    public function bindPassengers(array $order)
    {
        $cabinsPassengers = array_reduce($order['cabins'], function ($passengers, $cabin) {
            return array_merge($passengers, call_user_func_array(
                'array_merge',
                count($cabin['passengers']) ? [$cabin['passengers']] : []
            ));
        }, []);

        try {
            foreach($cabinsPassengers as $PassengerData) {
                \Log::channel('daily')->debug("PassengerData");
                \Log::channel('daily')->debug($PassengerData);

                $passenger = [
                    'booking_id' => $this->id,
                    'cabin_id' => $PassengerData['cabin_id'],
                    'sales_method' => $PassengerData['sales_method'],
                    'tariff_type' => $PassengerData['tariff_type'],
                    'cabin_bind' => $PassengerData['cabin_bind'],
                    'accommodation' => $PassengerData['accommodation'],
                    'seat_type' => $PassengerData['seat_type'],
                    'travel_way' => $PassengerData['travel_way'],
                    'tariff' => $PassengerData['tariff'],
                    'discounts' => $PassengerData['discounts'],
                    'base_tariff_id' => $PassengerData['base_tariff_id'],
                    'currency' => $PassengerData['currency'],
                    'base_currency' => Tariff::BASE_CURRENCY,
                    'ratio_to_base_currency' => CurrencyRatio::getCurrencyRatio($PassengerData['currency'], Tariff::BASE_CURRENCY),
                    'first_name' => $PassengerData['first_name'],
                    'last_name' => $PassengerData['last_name'],
                    'birth_date' => $PassengerData['birth_date'],
                    'nationality' => $PassengerData['nationality'],
                    'passport_id' => $PassengerData['passport_id'],
                    'sex' => $PassengerData['sex'],
                ];
                \Log::channel('daily')->debug('passenger data: ');
                \Log::channel('daily')->debug($passenger);

                $passenger = Passenger::create($passenger);
                $passenger->save();
                \Log::channel('daily')->debug('passenger sales_method: ');
                \Log::channel('daily')->debug($passenger->sales_method);
                if (!$PassengerData['vehicle']) { continue;}

                $this->bindPersonalVehicle([
                    'sales_method' => 'online',
                    'booking_number' => $this->number,
                    'vehicle_type' => $PassengerData['vehicle'],
                    'length' => $PassengerData['vehicle_length'],
                    'weight' => $PassengerData['vehicle_weight'],
                    'vehicle_make' => $PassengerData['vehicle_make'],
                    'vehicle_model' => $PassengerData['vehicle_model'],
                    'vin' => $PassengerData['vin'],
                    'reg_number' => $PassengerData['registration_number'],
                    'driver' => $passenger->id,
                    'currency' => $PassengerData['currency'],
                ]);
            }
        } catch (\Exception $e) {
            \Log::channel('daily')->debug($e->getMessage());
            throw new \DomainException('Failed to add passengers or vehicles to the booking! Error: ' . $e->getMessage());
        }
    }

    public static function currency($booking)
    {
        $currencyPassColl = Passenger::where('booking_id', $booking)
            ->select('currency')
            ->get();

        if ($currencyPassColl->isEmpty()) {
            return null;
        }
        $passengerCurrency = $currencyPassColl->first()->currency;
        foreach ($currencyPassColl as $item) {
            if ($item->currency !== $passengerCurrency) { $passengerCurrency = null; }
        }

        $currencyPersonalVehColl = Personal::where('booking_id', $booking)
            ->select('currency')
            ->get();
        if ($currencyPersonalVehColl->isEmpty()) {
            return $passengerCurrency;
        }
        $personalVehicleCurrency = $currencyPersonalVehColl->first()->currency;
        foreach ($currencyPersonalVehColl as $item) {
            if ($item->currency !== $personalVehicleCurrency) { $currency = null; }
        }

        if ($personalVehicleCurrency !== $passengerCurrency) {
            $currency = null;
        } else {
            $currency = $passengerCurrency = $personalVehicleCurrency;
        }

        return $currency;
    }

    public function getCurrency()
    {
        $currencyPassColl = Passenger::where('booking_id', $this->id)
            ->select('currency')
            ->get();

        if ($currencyPassColl->isEmpty()) {
            return null;
        }
        $passengerCurrency = $currencyPassColl->first()->currency;
        foreach ($currencyPassColl as $item) {
            if ($item->currency !== $passengerCurrency) { $passengerCurrency = null; }
        }

        $currencyPersonalVehColl = Personal::where('booking_id', $this->id)
            ->select('currency')
            ->get();
        if ($currencyPersonalVehColl->isEmpty()) {
            return $passengerCurrency;
        }
        $personalVehicleCurrency = $currencyPersonalVehColl->first()->currency;
        foreach ($currencyPersonalVehColl as $item) {
            if ($item->currency !== $personalVehicleCurrency) { $currency = null; }
        }

        if ($personalVehicleCurrency !== $passengerCurrency) {
            $currency = null;
        } else {
            $currency = $passengerCurrency = $personalVehicleCurrency;
        }

        return $currency;
    }

    public static function totalAmount($booking)
    {
        $bookingTotalInvoice = 0;
        $passengerTariffColl = Passenger::where('booking_id', $booking)
            ->select('tariff')
            ->get();
        if ($passengerTariffColl->isNotEmpty()) {
            $bookingTotalInvoice += $passengerTariffColl->sum('tariff');
        }

        $personalVehicleTariffColl = Personal::where('booking_id', $booking)
            ->select('tariff')
            ->get();
        if ($personalVehicleTariffColl->isNotEmpty()) {
            $bookingTotalInvoice += $personalVehicleTariffColl->sum('tariff');
        }

        return $bookingTotalInvoice;
    }

    public function getTotalAmount()
    {
        $bookingTotalInvoice = 0;
        $passengerTariffColl = Passenger::where('booking_id', $this->id)
            ->select('tariff')
            ->get();
        if ($passengerTariffColl->isNotEmpty()) {
            $bookingTotalInvoice += $passengerTariffColl->sum('tariff');
        }

        $personalVehicleTariffColl = Personal::where('booking_id', $this->id)
            ->select('tariff')
            ->get();
        if ($personalVehicleTariffColl->isNotEmpty()) {
            $bookingTotalInvoice += $personalVehicleTariffColl->sum('tariff');
        }

        return $bookingTotalInvoice;
    }

    public function getTransferPayment(string $currency)
    {
        if (!$currency = $this->getCurrency()) {
            throw new \DomainException('Booking Currency Error!');
        }

        $transferPayment = \DB::table('booking_payments as bp')
            ->select('bp.*')
            ->where('bp.order_batch', $this->order_batch)
            ->where('bp.booking_id', $this->id)
            ->where('bp.currency', $currency)
            ->where('bp.transaction_type', Payment::PAYMENT_TRANSACTION_TYPE)
            ->where('bp.payment_method', Payment::TRANSFER_PAYMENT_METHOD)
            ->whereExists(function ($query) {
                $query->select(\DB::raw(1))
                    ->from('booking_payments')
                    ->whereRaw('booking_payments.booking_id = bp.source')
                    ->whereRaw('booking_payments.transferred = bp.booking_id')
                    ->where('booking_payments.status', Payment::ACCEPTED_PAYMENT_STATUS)
                    ->whereExists(function ($q) {
                        $q->select(\DB::raw(1))
                            ->from('bookings')
                            ->whereRaw('bookings.id = booking_payments.booking_id')
                            ->where('status', PassengerBooking::REBOOKING_STATUS);
                    });
            })
            //->leftJoin('booking_payments as transferred', 'transferred.booking_id', '=', 'bp.source')
            /*->leftJoin(\DB::raw("(SELECT amount FROM booking_payments) AS t"),
                't.booking_id', '=', 'bp.source')*/
            //->select(\DB::raw('SELECT amount as transferred_amount FROM booking_payments WHERE booking_payments.booking_id = bp.source'))
            /*->addSelect(\DB::raw('amount as transferred_amount')
                ->whereExists(function ($q) {
                    $q->select(\DB::raw(1))
                        ->from('booking_payments')
                        ->whereRaw('booking_payments.booking_id = bp.source');
                })
            )*/
            ->get();
        if ($transferPayment->count() === 0) {
            return null;
        }
        if ($transferPayment->count() > 1) {
            throw new \DomainException('Found more than one transfer payment for 1 booking №' . $this->id);
        }

        return $transferPayment->first();
    }

    public function getDirectPayments(string $currency)
    {
        return \DB::table('booking_payments')
            ->where('booking_id', $this->id)
            ->where('currency', $currency)
            ->where('transaction_type', Payment::PAYMENT_TRANSACTION_TYPE)
            ->whereIn('payment_method', [
                Payment::AGENT_CREDIT_PAYMENT_METHOD,
                Payment::CARD_PAYMENT_METHOD,
            ])
            ->get()
            ->sum('amount');
    }

    public function getRefunds(string $currency)
    {
        return \DB::table('booking_payments')
            ->where('order_batch', $this->order_batch)
            ->where('booking_id', $this->id)
            ->where('transaction_type', Payment::REFUND_TRANSACTION_TYPE)
            ->where('currency', $currency)
            ->where('payment_method', Payment::MANAGER_REFUNDING_PAYMENT_METHOD)
            ->get()
            ->sum('amount');
    }

    public function getTransferPaymentBalance($booking_id, $currency)
    {
        $booking = PassengerBooking::where('id', $booking_id)->first();

        if (!$transfer = $booking->getTransferPayment($currency)) {
            return $booking->getDirectPayments($currency) - $booking->getRefunds($currency);
        }

        return $booking->getDirectPayments($currency)
            - $booking->getRefunds($currency)
            + $this->getTransferPaymentBalance($transfer->source, $currency);
    }

    public static function paidByOrder($order) {
        $payments = Payment::where('order_batch', $order)
            ->select(\DB::raw('booking_id'))
            ->groupBy('booking_id')
            ->get();

        if (!$payments->count()) {
            return 0;
        }

        if (count($payments->toArray()) > 1) {
            throw new \DomainException('FATAL! ERROR! Order ' . $order . ' belongs more than one booking!');
        }

        $booking = PassengerBooking::find($payments->first()->toArray()['booking_id']);
        return $booking->getPaymentBalance();
    }

    public static function paidByBooking($booking) {
        /*$payments = Payment::where('booking_id', $booking)
            ->select(\DB::raw('booking_id'))
            ->groupBy('booking_id')
            ->get();

        if (!$payments->count()) {
            return 0;
        }*/

        /*if (count($payments->toArray()) > 1) {
            throw new \DomainException('FATAL! ERROR! Order ' . $order . ' belongs more than one booking!');
        }*/

        //$booking = PassengerBooking::find($payments->first()->toArray()['booking_id']);
        $booking = PassengerBooking::find($booking);
        return $booking->getPaymentBalance();
    }

    public function getPaymentBalance()
    {
        if (!$currency = $this->getCurrency()) {
            throw new \DomainException('Booking Currency Error!');
        }

        $transfer = $this->getTransferPayment($currency);

        if (!$transfer) {
            return $this->getDirectPayments($currency) - $this->getRefunds($currency);
        }

        return $this->getTransferPaymentBalance($transfer->source, $currency) + $this->getDirectPayments($currency) - $this->getRefunds($currency);
    }

    public function canBeBooked()
    {
        $bookingPaymentBalance = $this->getPaymentBalance();

        $bookingNeedAmount = $this->getTotalAmount();
        if ($bookingPaymentBalance < $bookingNeedAmount * (self::ALLOWABLE_ORDER_BALANCE_PERCENT_FOR_CREATE / 100)) {
            return false;
        }
        return true;
    }

    public function canNotBeModifiedByOwner()
    {
        $allowableInterval = Settings\Booking::where('group_id', Settings\Booking::EXPIRING_EDIT_INTERVAL_TYPE)
            ->where('model_id', $this->boarding_port)
            ->first()->value;
        $departure = \DB::table('voyages_ports')
            ->where('voyage_id', $this->voyage_id)
            ->where('port_id', $this->boarding_port)
            ->first()->departure;

        if (Carbon::now()->addMinutes($allowableInterval)->gt(Carbon::createFromFormat('Y-m-d H:i:s', $departure))) {
            return 'Time for corrections expired';
        }
        if ($this->number_edit_attempts >= self::ALLOWABLE_NUMBER_EDIT_ATTEMPTS) {
            return 'Attempt limit exceeded. (max 3)';
        }
        if ($this->number_edit_attempts >= self::ALLOWABLE_NUMBER_EDIT_ATTEMPTS) {
            return 'Attempt limit exceeded. (max 3)';
        }
        return false;
    }

    public function getPDFCustomerConfirmation()
    {
        $fetcher = new PassengerBookingFetcher();
        $booking = $fetcher->byNumber($this->number, true);
        $user = \Auth::user();
        $agent = Agent::where('port_id', $booking['boarding_id'])->first();

        return \PDF::loadView('pdf.booking_confirmation.customer', ['user' => $user, 'booking' => $booking, 'agent' => $agent]);
    }

    public function getPDFInternalPurposesConfirmation()
    {
        $fetcher = new PassengerBookingFetcher();
        $booking = $fetcher->byNumber($this->number, true);
        $user = \Auth::user();
        $agent = Agent::where('port_id', $booking['boarding_id'])->first();

        return \PDF::loadView('pdf.booking_confirmation.internal_purposes', ['user' => $user, 'booking' => $booking, 'agent' => $agent]);
    }
}
